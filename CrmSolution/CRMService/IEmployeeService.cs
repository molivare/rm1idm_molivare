﻿using CRMService.DataContract;
using System.Collections.Generic;
using System.ServiceModel;

namespace CRMService
{
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        [FaultContract(typeof(ExceptionContract))]
        EmployeeContract GetEmployee(int employeeId);

        [OperationContract]
        ICollection<EmployeeContract> ListEmployees();

        [OperationContract]
        EmployeeContract AddEmployee(string initials, string phone);

        [OperationContract]
        [FaultContract(typeof(ExceptionContract))]
        void DeleteEmployee(int employeeId);

        [OperationContract]
        bool UpdateEmployee(int employeeId, string phone);
    }
}

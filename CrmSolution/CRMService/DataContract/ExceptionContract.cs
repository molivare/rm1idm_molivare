﻿using System;
using System.Runtime.Serialization;

namespace CRMService.DataContract
{
    [DataContract]
    public class ExceptionContract
    {
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public string ErrorDetails { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace CRMService.DataContract
{
    [DataContract]
    public class EmployeeContract
    {
        [DataMember]
        public int EmployeeId { get; set; }
        [DataMember]
        public string Initials { get; set; }
        [DataMember]
        public string Phone { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace CRMService.DataContract
{
    [DataContract]
    public class CustomerContract
    {
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public int RepId { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public EmployeeContract AccountExecutive { get; set; }
    }
}

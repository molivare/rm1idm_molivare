﻿using CRMService.DataContract;
using CRMService.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.ServiceModel;

namespace CRMService
{
    public class CustomerService : ICustomerService
    {
        public CustomerContract GetCustomer(int customerId)
        {
            CustomerContract contract = null;
            try
            {
                using (var context = new CRMDataEntities())
                {
                    customer customer = context.customers.First(x => x.customerId == customerId);
                    if (customer != null)
                    {
                        contract = _ConvertCustomerToContract(customer);
                    }
                }
            }
            catch (Exception ex)
            {
                var exception = new ExceptionContract();
                exception.ErrorMessage = ex.Message;
                throw new FaultException<ExceptionContract>(exception);
            }

            return contract;
        }

        public ICollection<CustomerContract> ListCustomers()
        {
            ICollection<CustomerContract> contracts = new List<CustomerContract>();

            using (var context = new CRMDataEntities())
            {
                var customers = context.customers.ToList();

                foreach(var item in customers)
                {
                    contracts.Add(_ConvertCustomerToContract(item));
                }
            }
            return contracts;
        }

        public CustomerContract AddCustomer(int repId, string accountNumber)
        {
            CustomerContract contract = null;

            using (var context = new CRMDataEntities())
            {
                customer customer = new customer();
                customer.repId = repId;
                customer.customerAccountNumber = accountNumber;

                var objContext = ((IObjectContextAdapter)context).ObjectContext;
                objContext.AddObject("customers", customer);

                context.SaveChanges();

                contract = _ConvertCustomerToContract(customer);
            }
            return contract;
        }

        public void DeleteCustomer(int customerId)
        {
            var customer = _GetCustomer(customerId);
            if (customer != null)
            {
                using (var context = new CRMDataEntities())
                {
                    var objContext = ((IObjectContextAdapter)context).ObjectContext;
                    objContext.AttachTo("customers", customer);
                    objContext.DeleteObject(customer);

                    context.SaveChanges();
                }
            }
        }

        public bool UpdateCustomer(int customerId, int repId)
        {
            var customer = _GetCustomer(customerId);
            if (customer != null)
            {
                using (var context = new CRMDataEntities())
                {
                    customer.repId = repId;
                    var objContext = ((IObjectContextAdapter)context).ObjectContext;
                    objContext.AddObject("customers", customer);
                    objContext.ObjectStateManager.ChangeObjectState(customer, System.Data.EntityState.Modified);

                    context.SaveChanges();

                    return true;
                }
            }
            return false;
        }

        #region private methods

        /// <summary>
        /// Gets the customer data
        /// </summary>
        /// <param name="customerId">customer identifier</param>
        /// <returns>customer entity</returns>
        private customer _GetCustomer(int customerId)
        {
            using (var context = new CRMDataEntities())
            {
                customer customer = new customer();
                customer = context.customers.First(x => x.customerId == customerId);
                return customer;
            }
        }

        /// <summary>
        /// Converts entity to contract
        /// </summary>
        /// <param name="employee">entity to convert</param>
        /// <returns>contract</returns>
        private CustomerContract _ConvertCustomerToContract(customer customer)
        {
            CustomerContract contract = new CustomerContract();
            contract.CustomerId = customer.customerId;
            contract.AccountNumber = customer.customerAccountNumber;
            contract.RepId = customer.repId;

            if (customer.employee != null)
            {
                contract.AccountExecutive = _ConvertEmployeeToContract(customer.employee);
            }

            return contract;
        }

        /// <summary>
        /// Converts entity to contract
        /// </summary>
        /// <param name="employee">entity to convert</param>
        /// <returns>contract</returns>
        private EmployeeContract _ConvertEmployeeToContract(employee employee)
        {
            EmployeeContract contract = new EmployeeContract();
            contract.EmployeeId = employee.employeeId;
            contract.Phone = employee.employeePhone;
            contract.Initials = employee.employeeInitials;

            return contract;
        }

        #endregion
    }
}

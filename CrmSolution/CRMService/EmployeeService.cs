﻿using CRMService.DataContract;
using CRMService.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.ServiceModel;

namespace CRMService
{
    public class EmployeeService : IEmployeeService
    {
        #region Public Methods

        /// <summary>
        /// Returns employee details by employee id
        /// </summary>
        /// <param name="employeeId">employee identifier</param>
        /// <returns>employee contract</returns>
        public EmployeeContract GetEmployee(int employeeId)
        {
            EmployeeContract contract = new EmployeeContract();
            try
            {
                var employee = _GetEmployee(employeeId);
                contract = _ConvertEmployeeToContract(employee);
            }
            catch (Exception ex)
            {
                var exception = new ExceptionContract();
                exception.ErrorMessage = ex.Message;
                throw new FaultException<ExceptionContract>(exception);
            }
            
            return contract;
        }

        /// <summary>
        /// Returns a list of employees
        /// </summary>
        /// <returns>list of employee contracts</returns>
        public ICollection<EmployeeContract> ListEmployees()
        {
            ICollection<EmployeeContract> contracts = new List<EmployeeContract>();

            using (var context = new CRMDataEntities())
            {
                var employees = context.employees.ToList();

                foreach (var item in employees)
                {
                    contracts.Add(_ConvertEmployeeToContract(item));
                }
            }
            return contracts;
        }


        /// <summary>
        /// Adds an employee record
        /// </summary>
        /// <param name="initials">employee initials</param>
        /// <param name="phone">employee phone number</param>
        /// <returns>employee contract</returns>
        public EmployeeContract AddEmployee(string initials, string phone)
        {
            EmployeeContract contract = null;

            using (var context = new CRMDataEntities())
            {
                employee employee = new employee();
                employee.employeeInitials = initials;
                employee.employeePhone = phone;

                var objContext = ((IObjectContextAdapter)context).ObjectContext;
                objContext.AddObject("employees", employee);

                context.SaveChanges();

                contract = _ConvertEmployeeToContract(employee);
            }

            return contract;
        }

        /// <summary>
        /// deletes the employee from the database
        /// </summary>
        /// <param name="employeeId">employee identifier</param>
        public void DeleteEmployee(int employeeId)
        {
            try
            {
                using (var context = new CRMDataEntities())
                {
                    employee employee = context.employees.First(x => x.employeeId == employeeId);
                    if (employee != null)
                    {
                        var objContext = ((IObjectContextAdapter)context).ObjectContext;
                        objContext.DeleteObject(employee);

                        context.SaveChanges();
                    }
                }
            }
            catch (UpdateException ex)
            {
                var exception = new ExceptionContract();
                exception.ErrorMessage = ex.Message;
                throw new FaultException<ExceptionContract>(exception);
            }
        }

        /// <summary>
        /// updates an employees phone number
        /// </summary>
        /// <param name="employeeId">employee identifier</param>
        /// <param name="phone">phone number</param>
        /// <returns>true if updated</returns>
        public bool UpdateEmployee(int employeeId, string phone)
        {
            var employee = _GetEmployee(employeeId);
            if(employee != null)
            {
                using (var context = new CRMDataEntities())
                {
                    employee.employeePhone = phone;
                    var objContext = ((IObjectContextAdapter)context).ObjectContext;
                    objContext.AddObject("employees", employee);
                    objContext.ObjectStateManager.ChangeObjectState(employee, System.Data.EntityState.Modified);
                    
                    context.SaveChanges();
                    
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region private Methods

        /// <summary>
        /// Gets the employee data
        /// </summary>
        /// <param name="employeeId">employee identifier</param>
        /// <returns>employee entity</returns>
        private employee _GetEmployee(int employeeId)
        {
            using (var context = new CRMDataEntities())
            {
                employee employee = new employee();
                employee = context.employees.First(x => x.employeeId == employeeId);
                return employee;
            }
        }
        
        /// <summary>
        /// Converts entity to contract
        /// </summary>
        /// <param name="employee">entity to convert</param>
        /// <returns>contract</returns>
        private EmployeeContract _ConvertEmployeeToContract(employee employee)
        {
            EmployeeContract contract = new EmployeeContract();
            contract.EmployeeId = employee.employeeId;
            contract.Phone = employee.employeePhone;
            contract.Initials = employee.employeeInitials;

            return contract;
        }

        #endregion
    }
}

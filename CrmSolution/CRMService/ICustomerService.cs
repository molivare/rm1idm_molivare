﻿using CRMService.DataContract;
using System.Collections.Generic;
using System.ServiceModel;

namespace CRMService
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        [FaultContract(typeof(ExceptionContract))]
        CustomerContract GetCustomer(int customerId);

        [OperationContract]
        ICollection<CustomerContract> ListCustomers();

        [OperationContract]
        CustomerContract AddCustomer(int repId, string accountNumber);

        [OperationContract]
        void DeleteCustomer(int customerId);

        [OperationContract]
        bool UpdateCustomer(int customerId, int repId);
    }
}

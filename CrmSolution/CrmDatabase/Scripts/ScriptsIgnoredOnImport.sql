﻿
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SELECT * INTO #tempEmployeeTable
FROM [dbo].[employees]
GO

SELECT * INTO #tempCustomersTable
FROM [dbo].[customers]
GO

ALTER TABLE [dbo].[customers]
DROP CONSTRAINT [FK_customers_employees]
GO

DROP TABLE [dbo].[customers]
GO

DROP TABLE [dbo].[employees]
GO

SET IDENTITY_INSERT [dbo].[employees] ON
GO

INSERT INTO [dbo].[employees](employeeId, employeeInitials, employeePhone)
SELECT employeeId, employeeInitials, employeePhone FROM #tempEmployeeTable
GO

DROP TABLE #tempEmployeeTable
GO

SET IDENTITY_INSERT [dbo].[employees] OFF
GO

SET IDENTITY_INSERT [dbo].[customers] ON
GO

INSERT INTO [dbo].[customers](customerId, repId, customerAccountNumber)
SELECT customerId, repId, customerAccountNumber FROM #tempCustomersTable
GO

DROP TABLE #tempCustomersTable
GO

SET IDENTITY_INSERT [dbo].[customers] OFF
GO

SET ANSI_PADDING OFF
GO

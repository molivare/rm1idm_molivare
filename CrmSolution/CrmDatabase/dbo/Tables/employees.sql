﻿CREATE TABLE [dbo].[employees] (
    [employeeId]       INT          NOT NULL,
    [employeeInitials] CHAR (3)     NOT NULL,
    [employeePhone]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_employees] PRIMARY KEY CLUSTERED ([employeeId] ASC)
);


﻿CREATE TABLE [dbo].[customers] (
    [customerId]            INT       NOT NULL,
    [repId]                 INT       NOT NULL,
    [customerAccountNumber] CHAR (10) NOT NULL,
    CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED ([customerId] ASC)
);

GO;

ALTER TABLE [dbo].[customers]
ADD FOREIGN KEY (repId)
REFERENCES [dbo].[employees](employeeId)
GO;

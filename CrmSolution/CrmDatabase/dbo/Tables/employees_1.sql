﻿CREATE TABLE [dbo].[employees](
	[employeeId] [int] NOT NULL IDENTITY(1,1),
	[employeeInitials] [char](3) NOT NULL,
	[employeePhone] [varchar](50) NOT NULL,
 CONSTRAINT [PK_employees] PRIMARY KEY CLUSTERED 
(
	[employeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
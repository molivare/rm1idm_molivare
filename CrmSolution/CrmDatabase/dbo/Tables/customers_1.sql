﻿CREATE TABLE [dbo].[customers](
	[customerId] [int] NOT NULL IDENTITY(1,1),
	[repId] [int] NOT NULL,
	[customerAccountNumber] [char](10) NOT NULL,
 CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
(
	[customerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customers]  ADD  CONSTRAINT [FK_customers_employees] FOREIGN KEY([repId])
REFERENCES [dbo].[employees] ([employeeId])
GO

ALTER TABLE [dbo].[customers] CHECK CONSTRAINT [FK_customers_employees]
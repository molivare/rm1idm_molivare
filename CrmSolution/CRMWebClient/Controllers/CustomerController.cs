﻿using CRMWebClient.CustomerServiceRef;
using EmployeeService = CRMWebClient.EmployeeServiceRef;
using CRMWebClient.Models;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web.Mvc;
using System;

namespace CRMWebClient.Controllers
{
    public class CustomerController : Controller
    {
        #region Fields

        private ICustomerService _customerService;
        private EmployeeService.IEmployeeService _employeeService;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor sets the service
        /// </summary>
        /// <param name="customerService">Service to set to the properties</param>
        public CustomerController(ICustomerService customerService, EmployeeService.IEmployeeService employeeService)
        {
            _customerService = customerService;
            _employeeService = employeeService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Index page for customers
        /// </summary>
        /// <returns>list of customers</returns>
        public ActionResult Index(string errorMessage = "")
        {
            ICollection<Customer> model = new List<Customer>();

            var customers = _customerService.ListCustomers();

            foreach (var item in customers)
            {
                var thisCustomer = new Customer();
                thisCustomer.customerId = item.CustomerId;
                thisCustomer.accountNumber = item.AccountNumber;
                thisCustomer.accountExecutiveId = item.RepId;
                thisCustomer.accountExecutive = _ConvertContractToEmployee(item.AccountExecutive);

                model.Add(thisCustomer);
            }

            ViewBag.errorMessage = errorMessage;
            return View(model);
        }

        /// <summary>
        /// Gets the details of the customer
        /// </summary>
        /// <param name="id">customer identifier</param>
        /// <returns>customer details</returns>
        public ActionResult Details(int id, string successMessage = "", string errorMessage = "")
        {
            string _errorMessage = errorMessage;
            string _successMessage = successMessage;

            Customer model = new Customer();
            try
            {
                var customer = _customerService.GetCustomer(id);
                model.customerId = customer.CustomerId;
                model.accountNumber = customer.AccountNumber;
                model.accountExecutiveId = customer.RepId;
                model.accountExecutive = _ConvertContractToEmployee(customer.AccountExecutive);
            }
            catch (FaultException<ExceptionContract>)
            {
                return RedirectToAction("Index", new { errorMessage = "That customer is no longer in our system." });
            }

            ViewBag.successMessage = _successMessage;
            ViewBag.errorMessage = _errorMessage;

            return View(model);   
        }

        /// <summary>
        /// Returns the create view for the customer
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Create()
        {
            CustomerCreate model = new CustomerCreate();
            model.employees = _GetEmployees();

            return View(model);
        }

        /// <summary>
        /// Processes the create request
        /// </summary>
        /// <param name="collection">form data</param>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var customer = _customerService.AddCustomer(Convert.ToInt32(collection["customer.accountExecutiveId"]), collection["customer.accountNumber"]);

                return RedirectToAction("Details", new { id = customer.CustomerId, successMessage = "The customer has been addedd succesfully." });
            }
            catch
            {
                ViewBag.errorMessage("There was an error adding the customer.");
                return View();
            }
        }

        /// <summary>
        /// Returns the edit screen for the customer
        /// </summary>
        /// <param name="id">customer identifier</param>
        /// <returns>edit view for customer</returns>
        public ActionResult Edit(int id)
        {
            CustomerCreate model = new CustomerCreate();
            try
            {
                var customer = _customerService.GetCustomer(id);
                model.customer.customerId = customer.CustomerId;
                model.customer.accountNumber = customer.AccountNumber;
                model.customer.accountExecutiveId = customer.RepId;
                model.customer.accountExecutive = _ConvertContractToEmployee(customer.AccountExecutive);

                model.employees = _GetEmployees();
            }
            catch (FaultException<ExceptionContract>)
            {
                return RedirectToAction("Index", new { errorMessage = "That customer is no longer in our system." });
            }

            return View(model);
        }
        /// <summary>
        /// Processes the edit submitssion
        /// </summary>
        /// <param name="id">the customer identifier</param>
        /// <param name="collection">form data</param>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var successMessage = "Employee has been updated.";
                var errorMessage = "";

                var updated = _customerService.UpdateCustomer(id, Convert.ToInt32(collection["customer.accountExecutiveId"]));

                if (!updated)
                {
                    errorMessage = "There was an error updating the customer.";
                    successMessage = "";
                }

                return RedirectToAction("Details", new { id = id, successMessage = successMessage, errorMessage = errorMessage });
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Deletes the customer
        /// </summary>
        /// <param name="id">customer identifier</param>
        /// <param name="collection">Form data</param>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _customerService.DeleteCustomer(Convert.ToInt32(collection["id"]));

                var successMessage = "Customer has been succesfully deleted.";
                return RedirectToAction("Index", new { successMessage = successMessage });
            }
            catch
            {
                return RedirectToAction("Index", new { errorMessage = "There was an error deleting the employee." });
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Converts contract to model
        /// </summary>
        /// <param name="contract">contract to convert</param>
        /// <returns>Employee</returns>
        private Employee _ConvertContractToEmployee(EmployeeContract contract)
        {
            Employee employee = new Employee();
            employee.employeeId = contract.EmployeeId;
            employee.phone = contract.Phone;
            employee.initials = contract.Initials;

            return employee;
        }

        /// <summary>
        /// Gets the list of employees
        /// </summary>
        /// <returns>returns the list of employees</returns>
        private IList<Employee> _GetEmployees()
        {
            var employees = new List<Employee>();
            var contracts = _employeeService.ListEmployees();

            foreach (var item in contracts)
            {
                var thisEmployee = new Employee();
                thisEmployee.employeeId = item.EmployeeId;
                thisEmployee.initials = item.Initials;

                employees.Add(thisEmployee);
            }

            return employees;
        }

        #endregion
    }
}

﻿using CRMWebClient.EmployeeServiceRef;
using CRMWebClient.Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web.Mvc;

namespace CRMWebClient.Controllers
{
    public class EmployeeController : Controller
    {
        #region Fields

        private IEmployeeService _employeeService;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor sets the service
        /// </summary>
        /// <param name="employeeService">Service to set to properties</param>
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Index page for employees
        /// </summary>
        /// <returns>list of employees</returns>
        public ActionResult Index(string errorMessage = "")
        {
            ICollection<Employee> model = new List<Employee>();

            var employees = _employeeService.ListEmployees();

            foreach(var item in employees)
            {
                var thisEmployee = new Employee();
                thisEmployee.employeeId = item.EmployeeId;
                thisEmployee.initials = item.Initials;
                thisEmployee.phone = item.Phone;

                model.Add(thisEmployee);
            }

            ViewBag.errorMessage = errorMessage;
            return View(model);
        }

        /// <summary>
        /// Gets the details of the employee
        /// </summary>
        /// <param name="id">employee identifier</param>
        /// <returns>employee detail</returns>
        public ActionResult Details(int id, string successMessage = "", string errorMessage = "")
        {
            string _errorMessage = errorMessage;
            string _successMessage = successMessage;

            Employee model = new Employee();
            try
            {
                var employee = _employeeService.GetEmployee(id);
                model.employeeId = employee.EmployeeId;
                model.initials = employee.Initials;
                model.phone = employee.Phone;
            }
            catch (FaultException<ExceptionContract>)
            {
                return RedirectToAction("Index", new { errorMessage = "That employee is no longer in our system." });
            }
            
            ViewBag.successMessage = _successMessage;
            ViewBag.errorMessage = _errorMessage;

            return View(model);
        }

        /// <summary>
        /// Returns the create view for the employee
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// processes the create request
        /// </summary>
        /// <param name="collection">submitted form collection</param>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var employee = _employeeService.AddEmployee(collection["initials"], collection["phone"]);

                return RedirectToAction("Details", new { id=employee.EmployeeId, successMessage = "The employee has been addedd succesfully." });
            }
            catch
            {
                ViewBag.errorMessage("There was an error adding the employee.");
                return View();
            }
        }

        /// <summary>
        /// Returns the edit screen for an employee
        /// </summary>
        /// <param name="id">employee identifier</param>
        /// <returns>edit view for employee</returns>
        public ActionResult Edit(int id)
        {
            Employee model = new Employee();
            try
            {
                var employee = _employeeService.GetEmployee(id);

                model.employeeId = employee.EmployeeId;
                model.initials = employee.Initials;
                model.phone = employee.Phone;
            }
            catch (FaultException<ExceptionContract>)
            {
                return RedirectToAction("Index", new { errorMessage = "That employee is no longer in our system." });
            }
            return View(model);
        }

        /// <summary>
        /// Edit submission
        /// </summary>
        /// <param name="id">employee identifier</param>
        /// <param name="collection">submitted changes</param>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var successMessage = "Employee has been updated.";
                var errorMessage = "";

                var updated = _employeeService.UpdateEmployee(id, collection["phone"]);

                if (!updated)
                {
                    errorMessage = "There was an error updating the employee.";
                    successMessage = "";
                }

                return RedirectToAction("Details", new { id = id,  successMessage = successMessage, errorMessage = errorMessage });
            }
            catch
            {
                return View();
            }
        }

       /// <summary>
       /// Deleted the employee
       /// </summary>
       /// <param name="id">employee identifier</param>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _employeeService.DeleteEmployee(Convert.ToInt32(collection["id"]));

                var successMessage = "Employee has been succesfully deleted.";
                return RedirectToAction("Index", new { successMessage = successMessage });
            }
            catch (FaultException<ExceptionContract> ex)
            {
                return RedirectToAction("Index", new { errorMessage = ex.Message });
            }
            catch
            {
                return RedirectToAction("Index", new { errorMessage = "There was an error deleting the employee." });
            }
        }

        #endregion
    }
}

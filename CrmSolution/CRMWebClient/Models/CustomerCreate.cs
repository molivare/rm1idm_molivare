﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CRMWebClient.Models
{
    public class CustomerCreate
    {
        #region Constructor

        public CustomerCreate()
        {
            customer = new Customer();
            employees = new List<Employee>();
        }

        #endregion

        #region Properties

        public Customer customer { get; set; }
        public IList<Employee> employees { get; set; }

        #endregion
    }
}
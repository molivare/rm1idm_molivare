﻿using System;
using System.ComponentModel;

namespace CRMWebClient.Models
{
    public class Employee
    {
        #region Properties

        public int employeeId { get; set; }

        [DisplayName("Employee Initials")]
        public string initials { get;set; }

        [DisplayName("Phone Number")]
        public string phone { get; set; }

        #endregion
    }
}
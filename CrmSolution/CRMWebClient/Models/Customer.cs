﻿using System;
using System.ComponentModel;

namespace CRMWebClient.Models
{
    public class Customer
    {
        #region Properties

        public int customerId { get; set; }

        [DisplayName("Account Number")]
        public string accountNumber {get;set;}

        [DisplayName("Account Executive ID")]
        public int accountExecutiveId { get; set; }

        [DisplayName("Account Executive")]
        public Employee accountExecutive { get; set; }

        #endregion
    }
}
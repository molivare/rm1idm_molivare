using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using CRMWebClient.EmployeeServiceRef;
using CRMWebClient.CustomerServiceRef;

namespace CRMWebClient
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // e.g. container.RegisterType<ITestService, TestService>();        
            container.RegisterType<IEmployeeService, EmployeeServiceClient>();
            container.RegisterType<ICustomerService, CustomerServiceClient>();

            return container;
        }
    }
}